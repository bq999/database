﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace system
{
    public partial class bug : Form
    {
        private SqlCeConnection mySqlConnection;

        public bug()
        {
            InitializeComponent();
            bugListBox();
        }

     

        public void bugListBox()
        {
            mySqlConnection = new SqlCeConnection(@"Data Source=C:\Users\c7129139\Desktop\MyBug.sdf ");

            //writes a sql query to select everything from the tbl_bug

            String sqlquery = "SELECT Username, Software, Description, Status FROM tblBug";

            SqlCeCommand mySqlCommand = new SqlCeCommand(sqlquery, mySqlConnection);

            try
            {
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();

                lbxbug.Items.Clear();

                while (mySqlDataReader.Read())
                {
                    // adds everything from database into a listview table, with sub boxes to space into columns
                    ListViewItem item1 = new ListViewItem(mySqlDataReader["Username"] + "");
                    item1.SubItems.Add(mySqlDataReader["Software"] + "");
                    item1.SubItems.Add(mySqlDataReader["Description"] + "");
                    item1.SubItems.Add(mySqlDataReader["Status"] + "");

                    lbxbug.Items.AddRange(new ListViewItem[] { item1 });

                }
                }



            catch (SqlCeException ex)
            {

                MessageBox.Show("Failure " + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void back_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin openAdmin = new admin();
            openAdmin.Show();
        }

       
       
    }
}
