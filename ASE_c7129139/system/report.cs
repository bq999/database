﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace system
{
    public partial class report : Form
    {

        SqlCeConnection mySqlConnection;

        public report()
        {
            InitializeComponent();
            reportBug();
        }


        public void reportBug()
        {
            mySqlConnection = new SqlCeConnection(@"Data Source=C:\Users\c7129139\Desktop/MyBug.sdf ");

            //writes a select sql statement to access everything
            String selcmd = "SELECT * FROM tblBug";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                //opens connection
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show("Failure" + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;
            //checks if empty
            if (string.IsNullOrEmpty(usernameBox.Text) ||
                string.IsNullOrEmpty(softwareBox.Text) ||
                string.IsNullOrEmpty(descriptionBox.Text))
            {
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }



        public void insertBugs(String Username, String Software, String Description, String Status, String commandString)
        {

            try
            {
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                //adds paprameters to textboxs to link with sql
                //cmdInsert.Parameters.AddWithValue("@ID", ID);
                cmdInsert.Parameters.AddWithValue("@username", Username);
                cmdInsert.Parameters.AddWithValue("@software", Software);
                cmdInsert.Parameters.AddWithValue("@description", Description);
                cmdInsert.Parameters.AddWithValue("@status", Status);
                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Succesfully reported your bug to the database!");
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(Username + " .." + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public void clearBugData()
        {
            usernameBox.Text = softwareBox.Text = descriptionBox.Text =  "";
        }




    

        private void reportBugButton_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                //writes an insert sql statement  to add all fields to databse
                String commandString = "INSERT INTO tblBug(Username, Software, Description, Status) VALUES (@username, @software, @description, @Status)";

                insertBugs(usernameBox.Text, softwareBox.Text, descriptionBox.Text, statusCombo.Text, commandString);
                reportBug();
                clearBugData();

                this.Hide();
                main openMain = new main();
                openMain.Show();    

            }

        }

        private void back_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            main openMain = new main();
            openMain.Show();  
        }





    }
}
