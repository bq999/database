﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace system
{
    public partial class admin : Form
    {
        public admin()
        {
            InitializeComponent();
        }

        private void report_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            bug openBug = new bug();
            openBug.Show();
        }

        private void logout_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 fl = new Form1();
            fl.Show();
        }
    }
}
