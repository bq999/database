﻿namespace system
{
    partial class register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.new_username = new System.Windows.Forms.Label();
            this.new_password = new System.Windows.Forms.Label();
            this.box_username = new System.Windows.Forms.TextBox();
            this.box_password = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.back_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(83, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(168, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "Register";
            // 
            // new_username
            // 
            this.new_username.AutoSize = true;
            this.new_username.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_username.Location = new System.Drawing.Point(28, 86);
            this.new_username.Name = "new_username";
            this.new_username.Size = new System.Drawing.Size(102, 25);
            this.new_username.TabIndex = 1;
            this.new_username.Text = "Username";
            // 
            // new_password
            // 
            this.new_password.AutoSize = true;
            this.new_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.new_password.Location = new System.Drawing.Point(28, 145);
            this.new_password.Name = "new_password";
            this.new_password.Size = new System.Drawing.Size(98, 25);
            this.new_password.TabIndex = 2;
            this.new_password.Text = "Password";
            // 
            // box_username
            // 
            this.box_username.Location = new System.Drawing.Point(184, 92);
            this.box_username.Name = "box_username";
            this.box_username.Size = new System.Drawing.Size(100, 20);
            this.box_username.TabIndex = 3;
            // 
            // box_password
            // 
            this.box_password.Location = new System.Drawing.Point(184, 151);
            this.box_password.Name = "box_password";
            this.box_password.Size = new System.Drawing.Size(100, 20);
            this.box_password.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 196);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Submit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // back_btn
            // 
            this.back_btn.Location = new System.Drawing.Point(55, 196);
            this.back_btn.Name = "back_btn";
            this.back_btn.Size = new System.Drawing.Size(75, 23);
            this.back_btn.TabIndex = 6;
            this.back_btn.Text = "Back";
            this.back_btn.UseVisualStyleBackColor = true;
            this.back_btn.Click += new System.EventHandler(this.back_btn_Click);
            // 
            // register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.ClientSize = new System.Drawing.Size(316, 246);
            this.Controls.Add(this.back_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.box_password);
            this.Controls.Add(this.box_username);
            this.Controls.Add(this.new_password);
            this.Controls.Add(this.new_username);
            this.Controls.Add(this.label1);
            this.Name = "register";
            this.Text = "register";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void button1_Click_1(object sender, System.EventArgs e)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label new_username;
        private System.Windows.Forms.Label new_password;
        private System.Windows.Forms.TextBox box_username;
        private System.Windows.Forms.TextBox box_password;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button back_btn;
    }
}