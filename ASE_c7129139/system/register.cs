﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace system
{
    public partial class register : Form
    {

        SqlCeConnection mySqlConnection;

        public register()
        {
            InitializeComponent();
            box_password.PasswordChar = '*';
            registerUser();
        }

        public void registerUser()
        {
            mySqlConnection = new SqlCeConnection(@"Data Source=I:\MyDatabaseSystem.sdf ");

            //establishes a connection
            String selcmd = "SELECT username, password FROM login";

            SqlCeCommand mySqlCommand = new SqlCeCommand(selcmd, mySqlConnection);

            try
            {
                //opens up connection to database
                mySqlConnection.Open();

                SqlCeDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
            }

            catch (SqlCeException ex)
            {

                MessageBox.Show("Failure" + ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        public bool checkInputs()
        {
            bool rtnvalue = true;
            //checks to see if inputs are empty
            if (string.IsNullOrEmpty(box_username.Text) ||
                string.IsNullOrEmpty(box_password.Text))
            { 
                MessageBox.Show("Error: Please check your inputs");
                rtnvalue = false;
            }

            return (rtnvalue);

        }

        public void insertRecord(String user, String pass, String commandString)
        {
            try
            {
                //makes a new command with the query created in commandstring
                SqlCeCommand cmdInsert = new SqlCeCommand(commandString, mySqlConnection);

                //adds fields from textbox to query
                cmdInsert.Parameters.AddWithValue("@username", user);
                cmdInsert.Parameters.AddWithValue("@password", pass);
                cmdInsert.ExecuteNonQuery();
                MessageBox.Show("Succesfully signed up, you can now log in!");
             //   this.Close();
            }
            catch (SqlCeException ex)
            {
                MessageBox.Show(" This username - " + user + " has already been used, try another!");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkInputs())
            {
                //makes a sql query to insert data
                String commandString = "INSERT INTO login(username, password) VALUES (@username, @password)";
                insertRecord(box_username.Text, box_password.Text, commandString);
                registerUser();

                this.Hide();


                main fm = new main();

                fm.Show();

            }

        }

        private void back_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 fm = new Form1();
            fm.Show();
        }
    }
}
