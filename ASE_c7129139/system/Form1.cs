﻿    using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlServerCe;

//-----------------------------------------------
// Author     : Bilal Qaiser
//-----------------------------------------------

namespace system
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txt_password.PasswordChar = '*';

        }

        string cs = @"Data Source=I:\MyDatabaseSystem.sdf";
        //string cs = @"Data Source=H:\login1.sdf";

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
             if(txt_username.Text=="" || txt_password.Text=="")
            {
                MessageBox.Show("Please provide username and password");
                return;
            }

            try

            {
                 //Create SqlConnection

                SqlCeConnection connection = new SqlCeConnection(cs);

                SqlCeCommand cmd = new SqlCeCommand("Select * from login where username=@username and password=@password", connection);

                cmd.Parameters.AddWithValue("@username",txt_username.Text);

                cmd.Parameters.AddWithValue("@password", txt_password.Text);

                connection.Open();

                SqlCeDataAdapter adapt = new SqlCeDataAdapter(cmd);

                DataSet ds = new DataSet();

                adapt.Fill(ds);

                connection.Close();

                int count = ds.Tables[0].Rows.Count;

                //If count is equal to 1, than show frmMain form

                if (count == 1)

                {
                    this.Hide();
                    main fm = new main();
                    fm.Show();

            }
            else
            {
                MessageBox.Show("Wrong username or password. Have you registered?");
            }
            }
                catch(Exception ex)

            {

                MessageBox.Show(ex.Message);

            }
        }

        private void btn_register_Click(object sender, EventArgs e)
        {
            this.Hide();

            register fm = new register();

            fm.Show();
        }

        private void admin_btn_Click(object sender, EventArgs e)
        {
            if (txt_username.Text == "admin" || txt_password.Text == "admin")
            {

                this.Hide();


                admin admin = new admin();

                admin.Show();

            }

            else
            {
                MessageBox.Show("You are not admin");
            }
        }

       
    }
    }


