﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace system
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private void btn_logout_Click(object sender, EventArgs e)
        {
            this.Hide();

            Form1 fl = new Form1();

            fl.Show();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {

            Application.Exit();

        }

        private void view_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            bug openBug = new bug();
            openBug.Show();
        }

        private void report_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            report openReport = new report();
            openReport.Show();
        }

        private void version_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
            version openVersion = new version();
            openVersion.Show();
        }

        private void main_Load(object sender, EventArgs e)
        {

        }

    }
}
