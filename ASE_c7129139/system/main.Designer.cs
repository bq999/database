﻿namespace system
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_logout = new System.Windows.Forms.Button();
            this.report_btn = new System.Windows.Forms.Button();
            this.version_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_logout
            // 
            this.btn_logout.Location = new System.Drawing.Point(79, 156);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(75, 23);
            this.btn_logout.TabIndex = 0;
            this.btn_logout.Text = "Log Out";
            this.btn_logout.UseVisualStyleBackColor = true;
            this.btn_logout.Click += new System.EventHandler(this.btn_logout_Click);
            // 
            // report_btn
            // 
            this.report_btn.Location = new System.Drawing.Point(79, 65);
            this.report_btn.Name = "report_btn";
            this.report_btn.Size = new System.Drawing.Size(75, 23);
            this.report_btn.TabIndex = 2;
            this.report_btn.Text = "Report Bugs";
            this.report_btn.UseVisualStyleBackColor = true;
            this.report_btn.Click += new System.EventHandler(this.report_btn_Click);
            // 
            // version_btn
            // 
            this.version_btn.Location = new System.Drawing.Point(63, 111);
            this.version_btn.Name = "version_btn";
            this.version_btn.Size = new System.Drawing.Size(109, 23);
            this.version_btn.TabIndex = 3;
            this.version_btn.Text = "Version Control";
            this.version_btn.UseVisualStyleBackColor = true;
            this.version_btn.Click += new System.EventHandler(this.version_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 31);
            this.label1.TabIndex = 4;
            this.label1.Text = "Welcome";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(233, 202);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.version_btn);
            this.Controls.Add(this.report_btn);
            this.Controls.Add(this.btn_logout);
            this.Name = "main";
            this.Text = "Welcome";
            this.Load += new System.EventHandler(this.main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_logout;
        private System.Windows.Forms.Button report_btn;
        private System.Windows.Forms.Button version_btn;
        private System.Windows.Forms.Label label1;
    }
}